/* Switchover settings */
const config = {
  wave: {
    sn: 'SiteNumber',
    title: 'Site',
    params: ['Hsig', 'Hmax', 'Tp', 'Tz'],
    sql: 'SELECT * from \"2bbef99e-9974-49b9-a316-57402b00609c\"',
    extra: [{}],
  },
  tide: {
    sn: 'SiteNumber',
    title: 'Site',
    params: ['Water Level', 'Prediction', 'Residual'],
    sql: 'SELECT * from \"7afe7233-fae0-4024-bc98-3a72f05675bd\"',
    extra: [{}],
  },
	/*
    extra: [
      {"siteAbreviation": "abellpoint","siteTitle": "Abell Point Marina","siteNumber": "101163","siteLatitude": "-20.260825","siteLongitude": "148.710329"},
      {"siteAbreviation": "aucklandpoint","siteTitle": "Gladstone (Auckland Point)","siteNumber": "052027a","siteLatitude": "0","siteLongitude": "0"},
      {"siteAbreviation": "boigu","siteTitle": "Boigu Island","siteNumber": "100221","siteLatitude": "-9.2437","siteLongitude": "142.254"},
      {"siteAbreviation": "bowen","siteTitle": "Bowen","siteNumber": "061007a","siteLatitude": "-20.0225","siteLongitude": "148.2514"},
      {"siteAbreviation": "bowenl","siteTitle": "Bowen LTG","siteNumber": "061007altg","siteLatitude": "-20.022517","siteLongitude": "148.25052"},
      {"siteAbreviation": "bundaberg","siteTitle": "Bundaberg","siteNumber": "051011a","siteLatitude": "-24.7704","siteLongitude": "152.3819"},
      {"siteAbreviation": "burketown","siteTitle": "Burketown","siteNumber": "6355b","siteLatitude": "-17.7167","siteLongitude": "139.5667"},
      {"siteAbreviation": "burnett","siteTitle": "Burnett Heads","siteNumber": "051010a","siteLatitude": "-24.7612","siteLongitude": "152.4005"},
      {"siteAbreviation": "cairns","siteTitle": "Cairns","siteNumber": "056012a","siteLatitude": "-16.9277","siteLongitude": "145.7788"},
      {"siteAbreviation": "cairnsc1","siteTitle": "Cairns C1","siteNumber": "056005a","siteLatitude": "0","siteLongitude": "0"},
      {"siteAbreviation": "caloundra","siteTitle": "NW1 Beacon - Caloundra","siteNumber": "100001","siteLatitude": "0","siteLongitude": "0"},
      {"siteAbreviation": "capeferg","siteTitle": "Cape Ferguson","siteNumber": "033007a","siteLatitude": "-19.2774","siteLongitude": "147.0585"},
      {"siteAbreviation": "cardwell","siteTitle": "Cardwell","siteNumber": "035012a","siteLatitude": "-18.2637","siteLongitude": "146.0262"},
      {"siteAbreviation": "clumppoint","siteTitle": "Clump Point","siteNumber": "035002a","siteLatitude": "-17.9515","siteLongitude": "146.1053"},
      {"siteAbreviation": "clumppoint","siteTitle": "Clump Point","siteNumber": "035002a","siteLatitude": "-17.9515","siteLongitude": "146.1053"},
      {"siteAbreviation": "cooktown","siteTitle": "Cooktown","siteNumber": "066003a","siteLatitude": "-15.4608","siteLongitude": "145.2487"},
      {"siteAbreviation": "courancove","siteTitle": "Couran Cove","siteNumber": "100983","siteLatitude": "-27.8245","siteLongitude": "153.4086"},
      {"siteAbreviation": "dalbay","siteTitle": "Dalrymple Bay","siteNumber": "100020","siteLatitude": "-21.2503","siteLongitude": "149.3044"},
      {"siteAbreviation": "dalrymplebay","siteTitle": "Dalrymple Bay MSQ","siteNumber": "060008a","siteLatitude": "0","siteLongitude": "0"},
      {"siteAbreviation": "dauan","siteTitle": "Dauan","siteNumber": "100267","siteLatitude": "-9.4112","siteLongitude": "142.5383"},
      {"siteAbreviation": "facingisland","siteTitle": "Facing Island","siteNumber": "100275","siteLatitude": "0","siteLongitude": "0"},
      {"siteAbreviation": "fisherlanding","siteTitle": "Fishermans Landing","siteNumber": "052003b","siteLatitude": "0","siteLongitude": "0"},
      {"siteAbreviation": "gcseaway","siteTitle": "Gold Coast Seaway","siteNumber": "045044a","siteLatitude": "-27.9348","siteLongitude": "153.4299"},
      {"siteAbreviation": "goldcoast","siteTitle": "Gold Coast","siteNumber": "040987a","siteLatitude": "-27.9385","siteLongitude": "153.4327"},
      {"siteAbreviation": "goldenbeach","siteTitle": "Golden Beach","siteNumber": "010011a","siteLatitude": "-26.8336","siteLongitude": "153.1196"},
      {"siteAbreviation": "halftidemsq","siteTitle": "Hay Point (Half Tide Tug Harbour)","siteNumber": "060010msq","siteLatitude": "0","siteLongitude": "0"},
      {"siteAbreviation": "hamiltonisland","siteTitle": "Hamilton Island","siteNumber": "101164","siteLatitude": "-20.350617","siteLongitude": "148.948688"},
      {"siteAbreviation": "haypointbeacon","siteTitle": "Hay Point Beacon #2","siteNumber": "100211","siteLatitude": "-21.2917","siteLongitude": "149.3033"},
      {"siteAbreviation": "humbugwharf","siteTitle": "Humbug Wharf","siteNumber": "070021msq","siteLatitude": "0","siteLongitude": "0"},
      {"siteAbreviation": "iama","siteTitle": "Iama","siteNumber": "100168","siteLatitude": "-9.8992","siteLongitude": "142.766"},
      {"siteAbreviation": "ib19weather","siteTitle": "IB19 Weather","siteNumber": "100208","siteLatitude": "0","siteLongitude": "0"},
      {"siteAbreviation": "jumpinpin","siteTitle": "Jumpinpin","siteNumber": "100985","siteLatitude": "-27.73853","siteLongitude": "153.43015"},
      {"siteAbreviation": "karumba","siteTitle": "Karumba","siteNumber": "071004b","siteLatitude": "-17.4883","siteLongitude": "140.8347"},
      {"siteAbreviation": "karumbabar","siteTitle": "Karumba Bar","siteNumber": "071007a","siteLatitude": "0","siteLongitude": "0"},
      {"siteAbreviation": "karumbawharf","siteTitle": "Karumba Wharf","siteNumber": "071004a","siteLatitude": "0","siteLongitude": "0"},
      {"siteAbreviation": "kubin","siteTitle": "Kubin","siteNumber": "100169","siteLatitude": "-10.236","siteLongitude": "142.2145"},
      {"siteAbreviation": "laguna","siteTitle": "Laguna Quay","siteNumber": "028008a","siteLatitude": "-20.6035","siteLongitude": "148.6801"},
      {"siteAbreviation": "lucinda","siteTitle": "Lucinda Offshore","siteNumber": "062006a","siteLatitude": "-18.5217","siteLongitude": "146.3853"},
      {"siteAbreviation": "lucindamsq","siteTitle": "Lucinda MSQ","siteNumber": "062006msq","siteLatitude": "0","siteLongitude": "0"},
      {"siteAbreviation": "lucindatemp","siteTitle": "Lucinda Temporary Gauge","siteNumber": "06002a","siteLatitude": "-18.5217","siteLongitude": "146.3853"},
      {"siteAbreviation": "mackay","siteTitle": "Mackay old","siteNumber": "054004a","siteLatitude": "-21.1029","siteLongitude": "149.2277"},
      {"siteAbreviation": "mackaynew","siteTitle": "Mackay","siteNumber": "054004nx","siteLatitude": "-21.1029","siteLongitude": "149.2277"},
      {"siteAbreviation": "maroochydore","siteTitle": "Maroochydore","siteNumber": "101204","siteLatitude": "-26.6431","siteLongitude": "153.0888"},
      {"siteAbreviation": "mooloolaba","siteTitle": "Mooloolaba","siteNumber": "011008a","siteLatitude": "-26.6836","siteLongitude": "153.1183"},
      {"siteAbreviation": "mooloolabamsq","siteTitle": "Mooloolaba MSQ","siteNumber": "011008msq","siteLatitude": "0","siteLongitude": "0"},
      {"siteAbreviation": "mornington","siteTitle": "Mornington Island old","siteNumber": "074011a","siteLatitude": "-16.6667","siteLongitude": "139.1671"},
      {"siteAbreviation": "morningtonA","siteTitle": "Mornington Island","siteNumber": "101043","siteLatitude": "-16.6","siteLongitude": "139"},
      {"siteAbreviation": "mossman","siteTitle": "Mossman","siteNumber": "041014a","siteLatitude": "-16.4358","siteLongitude": "145.4032"},
      {"siteAbreviation": "mourilyan","siteTitle": "Mourilyan","siteNumber": "063012a","siteLatitude": "-17.6012","siteLongitude": "146.1238"},
      {"siteAbreviation": "mourilyanmsq","siteTitle": "Mourilyan MSQ","siteNumber": "063012msq","siteLatitude": "0","siteLongitude": "0"},
      {"siteAbreviation": "noosagpb","siteTitle": "Noosa River Garth Prowd Bridge","siteNumber": "012022a","siteLatitude": "-26.3875167","siteLongitude": "153.0883"},
      {"siteAbreviation": "noosasand","siteTitle": "Noosa River Sand Jetty","siteNumber": "012023a","siteLatitude": "-26.383033","siteLongitude": "153.080216"},
      {"siteAbreviation": "noosatewantin","siteTitle": "Noosa River Tewantin","siteNumber": "012007e","siteLatitude": "-26.3874","siteLongitude": "153.0421"},
      {"siteAbreviation": "palmcove","siteTitle": "Palm Cove","siteNumber": "100421","siteLatitude": "-16.7398","siteLongitude": "145.6728"},
      {"siteAbreviation": "pinkenbawharf","siteTitle": "Brisbane River (Pinkenba Wharf)","siteNumber": "046047a","siteLatitude": "0","siteLongitude": "0"},
      {"siteAbreviation": "portalma","siteTitle": "Port Alma","siteNumber": "050008a","siteLatitude": "-23.5857","siteLongitude": "150.8618"},
      {"siteAbreviation": "portalmaltg","siteTitle": "Port Alma LTG","siteNumber": "050008ltg","siteLatitude": "-23.5857","siteLongitude": "150.8618"},
      {"siteAbreviation": "portdouglas","siteTitle": "Port Douglas","siteNumber": "100923","siteLatitude": "-16.4833","siteLongitude": "145.4667"},
      {"siteAbreviation": "port_douglas","siteTitle": "Port Douglas MSQ","siteNumber": "041015a","siteLatitude": "0","siteLongitude": "0"},
      {"siteAbreviation": "roeskamp","siteTitle": "Roes Kamp","siteNumber": "100989","siteLatitude": "-27.8993","siteLongitude": "153.419"},
      {"siteAbreviation": "rosslyn","siteTitle": "Rosslyn Bay","siteNumber": "024011a","siteLatitude": "-23.1667","siteLongitude": "150.7917"},
      {"siteAbreviation": "russellislande","siteTitle": "Russell Island East","siteNumber": "100984","siteLatitude": "-27.70955","siteLongitude": "153.3918"},
      {"siteAbreviation": "russellislandw","siteTitle": "Russell Island West","siteNumber": "101003","siteLatitude": "-27.7043","siteLongitude": "153.3611"},
      {"siteAbreviation": "s18weather","siteTitle": "S18 Weather","siteNumber": "100276","siteLatitude": "0","siteLongitude": "0"},
      {"siteAbreviation": "scarborough","siteTitle": "Scarborough","siteNumber": "048002b","siteLatitude": "-27.1936","siteLongitude": "153.1093"},
      {"siteAbreviation": "seaforth","siteTitle": "Seaforth","siteNumber": "101243","siteLatitude": "-20.901","siteLongitude": "148.9824"},
      {"siteAbreviation": "shorncliffe","siteTitle": "Shorncliffe Pier","siteNumber": "100903","siteLatitude": "-27.3217","siteLongitude": "153.087"},
      {"siteAbreviation": "shute","siteTitle": "Shute Harbour","siteNumber": "030003a","siteLatitude": "-20.2932","siteLongitude": "148.7862"},
      {"siteAbreviation": "southport","siteTitle": "Marine Ops Southport","siteNumber": "100035","siteLatitude": "0","siteLongitude": "0"},
      {"siteAbreviation": "southtrees","siteTitle": "Gladstone South Trees","siteNumber": "052026a","siteLatitude": "-23.8538","siteLongitude": "151.3137"},
      {"siteAbreviation": "sovereignislandnc","siteTitle": "Sovereign Island (North)","siteNumber": "100986","siteLatitude": "-27.8645","siteLongitude": "153.41"},
      {"siteAbreviation": "stpauls","siteTitle": "St Pauls","siteNumber": "100268","siteLatitude": "-10.57","siteLongitude": "142.3342"},
      {"siteAbreviation": "tangalooma","siteTitle": "Tangalooma Jetty","siteNumber": "101023","siteLatitude": "-27.178","siteLongitude": "153.371"},
      {"siteAbreviation": "thursdayisland","siteTitle": "Thursday Island","siteNumber": "057022b","siteLatitude": "-10.5863","siteLongitude": "142.2216"},
      {"siteAbreviation": "townsville","siteTitle": "Townsville","siteNumber": "055003a","siteLatitude": "-19.2518","siteLongitude": "146.8304"},
      {"siteAbreviation": "townsvillecard","siteTitle": "Townsville North Cardinal Beacon","siteNumber": "101343","siteLatitude": "-19.12663231","siteLongitude": "146.9094501"},
      {"siteAbreviation": "towntidemsq","siteTitle": "Townsville Tide MSQ","siteNumber": "055003msq","siteLatitude": "0","siteLongitude": "0"},
      {"siteAbreviation": "townweathermsq","siteTitle": "Townsville Weather","siteNumber": "222222","siteLatitude": "0","siteLongitude": "0"},
      {"siteAbreviation": "tweedsbj","siteTitle": "Tweed Sand Bypass Jetty","siteNumber": "1","siteLatitude": "-28.1721","siteLongitude": "153.5577"},
      {"siteAbreviation": "ugar","siteTitle": "Ugar","siteNumber": "100269","siteLatitude": "-9.5045","siteLongitude": "143.5468"},
      {"siteAbreviation": "urangan","siteTitle": "Urangan","siteNumber": "058009b","siteLatitude": "-25.2962","siteLongitude": "152.9105"},
      {"siteAbreviation": "wavebreaknc","siteTitle": "North Channel Wavebreak Island","siteNumber": "100988","siteLatitude": "-27.9318","siteLongitude": "153.4197"},
      {"siteAbreviation": "wavebreakwc","siteTitle": "Wavebreak Island (Labrador Channel)","siteNumber": "100987","siteLatitude": "-27.93815","siteLongitude": "153.4096"},
      {"siteAbreviation": "weipa","siteTitle": "Weipa Mace","siteNumber": "070021a","siteLatitude": "0","siteLongitude": "0"},
      {"siteAbreviation": "weipanx","siteTitle": "Weipa","siteNumber": "070021nx","siteLatitude": "-12.67","siteLongitude": "141.8623"},
      {"siteAbreviation": "whyteisland","siteTitle": "Brisbane Bar (Whyte Is.)","siteNumber": "046046a","siteLatitude": "0","siteLongitude": "0"},
      {"siteAbreviation": "whyteislandnx","siteTitle": "Whyte Island","siteNumber": "101183","siteLatitude": "-27.4017","siteLongitude": "153.1574"}
    ]},
	*/
};
var app_type = 'wave'; //Default app type unless changed

/* wrapper */
var wrapper = document.createElement('Div');
wrapper.setAttribute("class", "wrapper");
wrapper.setAttribute("id", "wrapper");
wrapper.setAttribute("name", "wrapper");
document.getElementsByTagName('body')[0].appendChild(wrapper);

/* Switch */
var switch_container = document.createElement('Div');
switch_container.setAttribute("class", "switch_container");
switch_container.innerHTML = "<div class='switch_text'>"
	+"<span id='wave_label' class='on_label'>Wave Data</span> "
	+"<label class='switch'> "
	+"<input id='switch_input' type='checkbox' onChange='switch_ui()'> "
	+"<span class='slider round'></span> "
	+"</label> "
	+"<span id='tide_label' class='off_label'>Tide Data</span> "
	+"</div>";
wrapper.appendChild(switch_container);

/* Progress bar */
var progress = document.createElement('Div');
progress.setAttribute("class", "progress");
progress.setAttribute("id", "progress");
progress.setAttribute("name", "progress");
progress.innerHTML = "<span>  Loading... Please wait</span>";
wrapper.appendChild(progress);


/* Display container */
var containerHeight = parseInt($(window).height()*0.5, 10);

var dpContainer = document.createElement("Div");
dpContainer.setAttribute("class", "container");
dpContainer.setAttribute("id", "container");
dpContainer.style.width = "100%";
dpContainer.style.height = (containerHeight>500) ? containerHeight : '500px' ;
dpContainer.style.minHeight = '500px';
document.getElementsByTagName('body')[0].appendChild(dpContainer);
/* popup */
var popup = document.createElement('Div');
popup.setAttribute('id', 'popup');
popup.setAttribute('name', 'popup');
popup.className = 'ol-popup';
popup.innerHTML = '<a href="#" id="popup-closer" class="ol-popup-closer"></a>'
                  +'<div id="popup-content"></div>';
dpContainer.appendChild(popup);
var container = document.getElementById('popup');
var content = document.getElementById('popup-content');
var closer = document.getElementById('popup-closer');
var overlay = new ol.Overlay({
  element: container,
  autoPan: true,
  autoPanAnimation: {
    duration: 250
  }
});
closer.onclick = function() {
  overlay.setPosition(undefined);
  closer.blur();
  return false;
};

/* Site Title Text */
var titleTxt = document.createElement("div");
titleTxt.innerHTML = "<p id='titleTxt' name='titleTxt' class='TitleText'></p>";
dpContainer.appendChild(titleTxt);
updateTitleTxt();

/* Open Layers Map */
var map;
var select = null; // ref to currently selected interaction
var coords = {};
var markers = {}; // Store key pair with 'sn' and ol marker object for all sites
var view = new ol.View({
      center: ol.proj.fromLonLat([153.03, -27.47]),
      zoom: 8
    });
var mapComp = document.createElement("Div");
mapComp.setAttribute("class", "map");
mapComp.setAttribute("id", "map");
mapComp.style.height = "300px";
dpContainer.appendChild(mapComp);
initMap("map");

/* Plot settings */
var lineColor = {};

/* Site selection */
var siteSelect = document.createElement("Select");
siteSelect.setAttribute('id','sn');
siteSelect.setAttribute('name','sn');
dpContainer.appendChild(siteSelect);

/* p_type invisible */
var ptypeSelect = document.createElement("Select");
ptypeSelect.setAttribute('id','p_type');
ptypeSelect.setAttribute('name','p_type');
dpContainer.appendChild(ptypeSelect);
ptypeSelect.style.display='none'; // Hide element for purpose


/* Add plot button */
var addDataButton = document.createElement("Button");
addDataButton.setAttribute('id','addPlotButton');
addDataButton.setAttribute('name','addPlotButton');
addDataButton.appendChild(document.createTextNode("Add to plot (+)"));
dpContainer.appendChild(addDataButton);
addDataButton.addEventListener("click", addToPlot);

/* Remove plot button */
var removeDataButton = document.createElement("Button");
removeDataButton.setAttribute('id','removeDataButton');
removeDataButton.setAttribute('name','removeDataButton');
removeDataButton.appendChild(document.createTextNode("Remove from plot (-)"));
dpContainer.appendChild(removeDataButton);
removeDataButton.addEventListener("click", removeFromPlot);

/* Reset button Plotly */ 
var resetButton = document.createElement("Button");
resetButton.setAttribute('id','resetButton');
resetButton.setAttribute('name','resetButton');
resetButton.appendChild(document.createTextNode("Reset"));
dpContainer.appendChild(resetButton);
resetButton.addEventListener("click", resetPlot);

/* Tab menu */
var tabContainer = document.createElement('Div');
tabContainer.setAttribute('id','tabContainer');
tabContainer.setAttribute('name','tabContainer');
tabContainer.className='tab';
var tabs=[]; // Global variable for tabs
init_tabs();

function init_tabs(){
  console.log('tabs: '+(typeof tabs.length));
  let tab_idx = tabs.length;
  for (let param of config[app_type].params){
    tabs.push(createTab('tab'+tab_idx, param));
  }
  for (let tab of tabs){
    registerTab(tab);
    tabContainer.appendChild(tab);
  }
  if (!dpContainer.contains(tabContainer)) dpContainer.appendChild(tabContainer);
  tabs[0].click(); //Default choice
}
/* End of Tab menu */

/* Add Plotly div */
var myDiv = document.createElement('Div');
myDiv.setAttribute('id','myDiv');
myDiv.setAttribute('name','myDiv');
myDiv.style.minHeight='500px';
//myDiv.style.height='500px';
dpContainer.appendChild(myDiv);
init_plotly();

/* Footer */
var footer = document.createElement('Div');
var year = new Date().getFullYear();
year = (year>=2019) ? year : 2019 ; // No previous year allowed
footer.innerHTML = '<div class="footer">'
          +'<span class="wrapblock">'
          +'</span>'
          +'<span class="wrapblock">'
            +'<div>'
            +'<a rel="license" target="_blank" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a>This work is licensed under a <a rel="license" target="_blank" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.'
            +'</div>'
            +'<p>'
            +' Kenneth Fung '
            +(year)
            +'<br>'
            +' All rights reserved.<br>'
            +'</p>'
          +'</span>'
          +'</div>';
dpContainer.appendChild(footer);

// User built-in source object instead
var source = {};
source.data = {};
source.data.x = [];
source.data.y = [];
source.data.s = [];

var snList = []; // siteNumber
var siteList = []; // Site
var selectedSite = [];
//var lines = {};
/*
var lines = [];
for (let param of config[app_type].params){
  lines[param] = {};
}
*/

var data_sites = []; // Store plotly trace when added

/* Data initialization */
var sorted_records; // Global sorted record set ready for plotting
var r;
function init_data(){
  let data = {
    sql: config[app_type].sql,
  };
  r = $.ajax({
    url: 'https://data.qld.gov.au/api/action/datastore_search_sql',
    data: data,
    dataType: 'jsonp',    
    success: function(data) {
      console.log('SQL: '+config[app_type].sql);
      let raw_records = add_extra_col(r.responseJSON.result.records);
      let filtered_records = filter_recs(raw_records); // Filter out error records
      sorted_records = sort_recs(filtered_records); // Sort all at once
      console.log('responseJSON length: ' + sorted_records.length);
      //console.log('data length: ' + data.result.records.length);
      populateSiteList(sorted_records);
      populatePType();
      gather_coords(sorted_records);
      console.log('coords length: '+Object.keys(coords).length);
      add_markers();
      //Make plot ready for showing
      addDataButton.click();
      resetPlot(true);
      console.log('sorted_records len: '+sorted_records.length);
    },
    beforeSend: function (XMLHttpRequest) {
      $('.progress').css({ 'width': '15%', 'min-width': '100px' }).show();
    },
    complete: function () {
        $('.progress').css({ 'width': parseInt($(window).width() * 0.6,10) }).delay(500).fadeOut();
    },    
    fail: function(data) {
      console.log('Failed to load...');
    }
  });
}

(function(){
  init_data();
})();

// Filter records within number of day period
function filter_by_period(recs, sn, days=7){
  let tmp_recs = [];
  let max_date;
  let min_date;
  if (app_type=='tide'){
    // Find max_date by sn
    recs.forEach(function(o){ 
      if (o.SiteNumber==sn){
        let dt = new Date(o.DateTime);
        if (max_date==null){
          max_date = dt;
        } else {
          if (max_date<dt) max_date = dt;
        }
      }
    });
    // Find min date
    min_date = max_date;
    min_date.setDate(min_date.getDate() - days);
    console.log('min_date:'+min_date);
    // Append record according to sn and min_date
    recs.forEach(function(o){ 
      if ((o.SiteNumber==sn) && (new Date(o.DateTime)>=min_date)){
        tmp_recs.push(o);
      }
    });
    
  } else {
    tmp_recs = recs;
  }

  return tmp_recs; 
}

// Add col SiteNumber to Tide records if missing
function add_extra_col(recs){
  let tmp_recs = [];
  let extra_sn = [];
  if (app_type=='tide'){
    config[app_type].extra.forEach(function(o){ extra_sn.push(o.siteAbreviation); });
  }
  //console.log(typeof recs);
  for (let rec of recs){
    //console.log(rec);
    let tmp_rec = rec;
    let site_idx;
    if (app_type == 'tide') {
      let siteLatitude, siteLongitude;
      if ((site_idx=extra_sn.indexOf(tmp_rec.Site)) != -1){
        // Use siteAbreviation as SiteNumber if matched one in config.tide.extra array
        let matched_site = config[app_type].extra[site_idx];
        tmp_rec.Site = matched_site.siteTitle; // Replace Site with siteTitle if exist
        tmp_rec.SiteNumber = matched_site.siteAbreviation;
        siteLatitude = parseFloat((matched_site.siteLatitude || matched_site.Latitude), 10);
        siteLongitude = parseFloat((matched_site.siteLongitude || matched_site.Longitude), 10);
      } else {
        tmp_rec.SiteNumber = tmp_rec.Site;
        siteLatitude = parseFloat((tmp_rec.siteLatitude || tmp_rec.Latitude), 10);
        siteLongitude = parseFloat((tmp_rec.siteLongitude || tmp_rec.Longitude), 10);
      }
      tmp_rec.Latitude = siteLatitude;
      tmp_rec.Longitude = siteLongitude;
    }
    tmp_recs.push(tmp_rec);
  }
  return tmp_recs;
}

function gather_coords(recs){
  let lat, lng;
  coords = {}; // Cleanup
  for (let i=0; i<recs.length; i++){
    let rec = recs[i];
    if (!(rec.SiteNumber in coords)){
      lat = (typeof rec.Latitude === 'undefined') ? '' : rec.Latitude;
      lng = (typeof rec.Longitude === 'undefined') ? '' : rec.Longitude;
      let strSite = rec[config[app_type].title];
      let sn = (rec[config[app_type].sn]).toString();
      coords[sn] = {
        'Latitude':lat,
        'Longitude':lng,
        'Site':strSite,
        'SiteNumber':sn
      };
      //console.log(coords[rec.SiteNumber]);
    }
  }
}

function flyTo(sn_selected){
  let duration = 1000;
  let zoom = view.getZoom();
  let coord = getCoordFromSN(sn_selected);
  console.log('fltTo -> Coord: '+coord.lng+','+coord.lat);
  let lng = coord.lng;
  let lat = coord.lat;
  let location = ol.proj.fromLonLat([lng, lat]);
  console.log('flyTo -> location: '+location);
  view.animate({
    center: location,
    duration: duration
  });
  /*
  view.animate({
    zoom: zoom - 1,
    duration: duration / 2
  }, {
    zoom: zoom,
    duration: duration / 2
  });
  */
}

function getCoordFromSN(sn_selected){
  let lng, lat;
  let coord = coords[sn_selected];
  return {
    lng: parseFloat(coord.Longitude),
    lat: parseFloat(coord.Latitude)
  };
}

function add_markers(){
  for (let key in coords){
    let coord = {
      'Longitude': coords[key]['Longitude'],
      'Latitude': coords[key]['Latitude'],
      'Site': coords[key]['Site'],
      'SiteNumber': coords[key]['SiteNumber']
    };
    console.log('Add marker for site: ' + coord.Site);
    add_marker_on_map(coord);
  }
  //addPointInteraction();
}

//Adding a marker on the map
function add_marker_on_map(coord){
  let lng = parseFloat(coord['Longitude']);
  let lat = parseFloat(coord['Latitude']);
  let stitle = coord['Site'];
  let sn = coord['SiteNumber'];
  console.log('Add point: '+lng+' , '+lat);
  let marker = new ol.Feature({
    geometry: new ol.geom.Point(
      ol.proj.fromLonLat([lng, lat])
    ),
  });
  marker.setProperties({
    'Longitude': lng,
    'Latitude': lat,
    'Site': stitle, 
    'SiteNumber': sn
  });
  let vSrc = new ol.source.Vector({
    features: [marker]
  });
  let l1 = new ol.layer.Vector({
    source: vSrc,
  });
  markers[sn] = marker;
  setPointStyle(l1);
  map.addLayer(l1);
}

function setPointStyle(l){
  // Styling
  let fill = new ol.style.Fill({
    color: [180, 0, 0, 0.3]
  });
  let stroke = new ol.style.Stroke({
    color: [180, 0, 0, 1],
    width: 1
  });
  let style = new ol.style.Style({
    image: new ol.style.Circle({
      fill: fill,
      stroke: stroke,
      radius: 8
    }),
    fill: fill,
    stroke: stroke
  });
  l.setStyle(style);
}


function styleClick(){
  // `this` is ol.Feature
  // Styling
  let fill = new ol.style.Fill({
    color: [255, 165, 0, 0.3]
  });
  let stroke = new ol.style.Stroke({
    color: [255, 165, 0, 1],
    width: 1
  });
  let style = new ol.style.Style({
    image: new ol.style.Circle({
      fill: fill,
      stroke: stroke,
      radius: 8
    }),
    fill: fill,
    stroke: stroke
  });
  return [ style ];
};

function addPointInteraction(){  
  console.log('Event addPointInteraction happened.')
  if (select != null) map.removeInteraction(select);
  let selectSingleClick = new ol.interaction.Select();
  // select interaction working on "pointermove"
  let selectPointerMove = new ol.interaction.Select({
    condition: ol.events.condition.pointerMove
  });
  // select interaction working on "click"
  let selectClick = new ol.interaction.Select({
    condition: ol.events.condition.click
  }); 
  
  select = selectClick;
  map.addInteraction(select);
  select.on('select', ClickToPlot);
  // Highlight selected point when clicked
  select.on('select', highlightSelectedPoint);  
  // Showing tooltips
  map.addOverlay(overlay);
  map.on('pointermove', displayTooltip);  
  
}

function highlightPointsBySelectedSite(){
  let snList = Object.keys(markers);
  for (let sn of snList){
    if (selectedSite.indexOf(sn.toString()) != -1 ){ // In case of sn selected
      console.log('highlightPointBySelectedSite -> sn '+sn+' highlighted.');
      markers[sn].setStyle(styleClick);
    } else {
      markers[sn].setStyle(null);
    }
  }
  select.getFeatures().clear();
}

function highlightSelectedPoint(e){ 
  // Highlight all points at once via screening global var selectedSite
  highlightPointsBySelectedSite();
}

function sort_recs(recs){
  recs.sort(function(a,b){
    let x = a.SiteNumber.toLowerCase(), y = b.SiteNumber.toLowerCase();
    if (a.SiteNumber === b.SiteNumber){
      return (parseInt(a.Seconds, 10) - parseInt(b.Seconds, 10));
    } else if (a.SiteNumber < b.SiteNumber) {
      return -1;
    } else {
      return 1;
    }
  });
  console.log('Fn sort_recs called...');
  return recs;
}

function filter_recs(recs){
    let tmp_recs = [];
    if (app_type=='wave'){
        recs.forEach(function(o){ 
          if (
            (o.Hsig<=5) &&
            (o.Hmax<=10) &&
            (o.Tp>=2) &&
            (o.Tp<=16) &&
            (o.Tz<=14)
          ){
            tmp_recs.push(o);
          }
        });    
    } else {
        tmp_recs = recs;
    }
    return tmp_recs;
}

function line_exist(sn){
  let line_exist = false;
  for (let k in selectedSite){ if (selectedSite[k]==sn) line_exist = true; };
  return line_exist;
}

function addPoints_All(recs, sn, ignoreSelCheck=false){
  let p_type = ptypeSelect[ptypeSelect.selectedIndex].value;
  addPoints_by_type(recs, sn, p_type, ignoreSelCheck);
}

function addPoints_by_type(recs, sn, p_type, ignoreSelCheck=false){ 
  console.log('DEBUG: sn:'+sn+', p_type:'+p_type+', ignoreSelCheck:'+ignoreSelCheck+', length:'+recs.length);
  
  //let tmp_recs = filter_by_period(recs, sn);
  let tmp_recs = recs;
                   
  source.data.x = [];
  source.data.y = [];
  source.data.s = [];
  let siteTitle;
  
  let ymin=0, ymax=0;

  
  for (let i=0; i<tmp_recs.length; i++){
    let rec = tmp_recs[i];
    let cnt = 0;
    if (sn == rec.SiteNumber){
      let y_val =  parseFloat(rec[p_type]);
      let dt = new Date(0); // Since Epoch
      dt.setUTCSeconds(rec.Seconds);
      source.data.x.push(dt);
      source.data.y.push((y_val==-99 || ( (p_type=='Residual') ? false : (y_val==0))) ? null: y_val);
      source.data.s.push(rec.Seconds);
      if (!siteTitle) siteTitle = rec.Site;
      
      ymin = (y_val<ymin) ? y_val : Math.ceil(ymin) ;
      ymax = (y_val>ymax) ? y_val : Math.ceil(ymax) ;
    }
  }
  let null_found = source.data.y.filter(function(e){return (e===null);}).length;
  if (null_found>0) console.log('WARNING:\n\n'+null_found+' error point(s) detected and removed from site '+siteTitle+'.');
  
  let line_id = 'line' + sn;
  let line_existed = false;
  line_existed = line_exist(sn);
  if (ignoreSelCheck || !line_existed){
    
    selectedSite.push(sn);

    //Only plot once at a time
    add_trace_to_plotly(siteTitle, source.data.x, source.data.y, p_type, ymin, ymax);   
    
  } else {
    alert('Reminder:\n\nData for site ' + siteTitle + ' has been plotted.\n\n');
  }

  
}

function populatePType(){
  let sorted_key = config[app_type].params;
  let o;      
  sorted_key.forEach(function(k){    
    o = document.createElement('option');
    o.value = k;
    o.text = k;
    ptypeSelect.appendChild(o);
  });    
}

function populateSiteList(recs){
  console.log('recs length: ' + recs.length);
  let tmp_recs={};
  for (let i=0;i<recs.length;i++){
    let sn = (recs[i].SiteNumber).toString();
    if (!(sn in tmp_recs)){
      // Add value pair with Site as unique key
      tmp_recs[recs[i].Site] = sn;
    }
  }
  //console.log('tmp_recs len:' + tmp_recs.length);
  /* Sorting by keys */
  let sorted_key = Object.keys(tmp_recs).sort();
  let o;      
  sorted_key.forEach(function(k){    
    o = document.createElement('option');
    o.value = tmp_recs[k];
    o.text = k;
    siteSelect.appendChild(o);
  });  
}

function getSelectedSN(){
  let val = (siteSelect[siteSelect.selectedIndex].value).toString();
  return val;  
}

function addToPlot(){
  let sn_selected = getSelectedSN();
  addPoints_All(sorted_records, sn_selected);  
  flyTo(sn_selected);
}

function removeFromPlot(){
  let sn_selected = getSelectedSN();
  removeLine(sn_selected);
  clearTracesOnly();
  recallPlot();
  flyTo(sn_selected);
}

function removeLine(sn_selected){
  let tmp_site = selectedSite.filter(function(value, index, arr){
    return value!=sn_selected;
  });
  selectedSite = tmp_site;
  return false;
}

function initMap(div_id){
  map = new ol.Map({
    target: div_id,
    layers: [
      new ol.layer.Tile({
        preload: 8,
        source: new ol.source.OSM()
      })
    ],
    view: view
  });
 
  addPointInteraction(); // Just initialize once
}

function displayTooltip(e){
  let pixel = e.pixel;
  let feature = map.forEachFeatureAtPixel(pixel, function(feature) {
    return feature;
  });
  container.style.display = feature ? '' : 'none';
  if (feature) {
    //console.log('e.coord: '+e.coordinate);
    //console.log('feature: '+Object.getOwnPropertyNames(feature));
    overlay.setPosition(e.coordinate);
    for (let k in Object.getOwnPropertyNames(feature)){
      container.innerHTML = '';
      container.innerHTML += 'Site: ' + feature.get('Site') + '<br>';
      //container.innerHTML += 'Site number: ' + feature.get('SiteNumber') + '<br>';
      container.innerHTML += 'Longitude: ' + feature.get('Longitude') + '<br>';
      container.innerHTML += 'Latitude: ' + feature.get('Latitude') + '<br>';
    }
  }
}

function ClickToPlot(e){
  let curr_selected_features = e.target.getFeatures();
  let last_selected_feature = e.selected;
  let last_deselected_feature = e.deselected;
  let fea = curr_selected_features.getArray()[0];
  if (typeof fea !== 'undefined') {
    console.log('selectClick -> Site: '+fea.get('Site'));
    console.log('selectClick -> SN: '+fea.get('SiteNumber'));
    console.log('selectClick -> Lng: '+fea.get('Longitude'));
    console.log('selectClick -> Lat: '+fea.get('Latitude'));
    let sel = document.getElementById('sn');
    sel.value = fea.get('SiteNumber');
    if (selectedSite.indexOf((sel.value).toString())!=-1){
      document.getElementById('removeDataButton').click();
    } else {
      // Change point style to selected state here...
      document.getElementById('addPlotButton').click();
    }
  }
 
}

function getRandHexColorCode(){
  return ('#' + parseInt(Math.random() * 0xffffff).toString(16));
}

// Compare and output color not in color list
function getRandomColor(lname){
  let rColor = getRandHexColorCode();
  //while (lineColor.indexOf(rColor)!=-1){
  if (lineColor.hasOwnProperty(lname)){  
    rColor = lineColor[lname]; // Retrieve color from global if exist
  } else {
    // Generate random color til unmatched
    while ((Object.values(lineColor)).indexOf(rColor)!=-1){
      rColor = getRandHexColorCode();
    }
    lineColor[lname] = rColor; // Assign color to global
  }
  return rColor;
}

/* Plotly.js */
function dt_to_str(dt){
  let d = new Date(dt);
  let yr = d.getFullYear();
  let mth = d.getMonth()+1;
  let day = d.getDate();
  mth = (mth>9 ? '' : '0') + mth;
  day = (day>9 ? '' : '0') + day;
  let hr = d.getHours();
  let minu = d.getMinutes();
  let sec = d.getSeconds();
  minu = (minu>9 ? '' : '0') + minu;
  sec = (sec>9 ? '' : '0') + sec;
  let s = yr+'-'+mth+'-'+day+' '+hr+':'+minu+':'+sec;
  //console.log('s: '+ s);
  return s;
}

function make_plotly_trace(lname, lx, ly, lcolor=null){
  let c = (lcolor==null) ? getRandomColor(lname) : lcolor;
  let lx_str = lx.map(dt_to_str);
  let trace = {
    type: "scatter",
    mode: "lines",
    name: lname,
    x: lx_str,
    y: ly,
    line: {color: c},
    showlegend: true
  };
  return trace;
}

function add_trace_to_plotly(siteTitle, lx, ly, p_type, ymin, ymax){
  //var data = [];
  let m_list = ['Hsig', 'Hmax', 'Water Level', 'Prediction', 'Residual'];
  let lname = siteTitle;
  let trace1 = make_plotly_trace(lname, lx, ly);
  data_sites.push(trace1);
  console.log('Trace '+lname+' added.');
  //Plotly.update('myDiv', data);
  let y_axis_title = (m_list.indexOf(p_type)!=-1) ? 'Height (m'+((app_type=='wave')?'':' LAT')+')' : 'Period (second)';
  let layout = {
    title: p_type,
    width: document.body.clientWidth,
    height: getPlotHeight(),
    xaxis: {
      autorange: true,
      zeroline: false,
    },
    yaxis: {
      yaxis: {range: [0, ymax]},
      autorange: true,
      zeroline: false,
      title: y_axis_title,
    }
  };
  Plotly.newPlot(myDiv, data_sites, layout);
  highlightPointsBySelectedSite();
}

function init_plotly(){
  let data = [
    {
      x: [],
      y: [],
      type: 'scatter'
    }
  ];
  let layout = {
    xaxis: {
      autorange: true,
      zeroline: false,
    },
    yaxis: {
      autorange: true,
      zeroline: false,
      titlefont: {
        size: 14,
        color: '#7f7f7f',
        family: 'Arial, sans-serif',
      }
    },
    width: document.body.clientWidth,
    height: getPlotHeight(),    
  };  

  Plotly.newPlot('myDiv', data, layout);  
}

function clearTracesOnly(){
  while(myDiv.data.length>0)
  {
    Plotly.deleteTraces(myDiv, [0]);
  }  
  highlightPointsBySelectedSite();
}

function clearSelectedSitePoints(){
  //for (k in lines) { lines[k] = {}; };
  selectedSite = [];  
  highlightPointsBySelectedSite();
}

function resetPlot(skipConfirm=false){
  if (skipConfirm==true){
    clearTracesOnly();
    clearSelectedSitePoints();
  } else {
    let sureFlag = confirm('Confirmation:\n\nAre you sure to reset this plot?');
    if (sureFlag){
      clearTracesOnly();
      clearSelectedSitePoints();
    } else {
      return false;
    }    
  }
}

function refreshCompHeight(){
  let plot_height = getPlotHeight();
  dpContainer.style.height = plot_height+'px';
  dpContainer.style.minHeight = '500px';
  mapComp.style.height = '300px';
  mapComp.style.minHeight = '300px';
  console.log('Set component height to '+plot_height);
}

function refreshPlot(){
  let update = {
                width: $('#tabContainer').outerWidth(),
                height: getPlotHeight(),
  };
  Plotly.relayout(myDiv, update);
  console.log('Refresh plot...');
}

function getPlotHeight(){
    containerHeight =  parseInt($(window).height()*0.5, 10); //Update global variable
    return (containerHeight>500) ? containerHeight : '500px' ;
}

function recallPlot(){
  let len = siteSelect.children.length;
  for (let i=0; i<len; i++){
    let sn_tmp = siteSelect[i].value;
    if (selectedSite.indexOf(sn_tmp)!=-1){
      console.log('Site '+sn_tmp+' selected previously, go plotting...');
      addPoints_All(sorted_records, sn_tmp, true); //Only plot if sn selected before
    } else {
      console.log('Site '+sn_tmp+' not previously selected.');
    }
  }  
  highlightPointsBySelectedSite();
}

// Listen for resize changes
window.addEventListener("resize", function() {
  refreshCompHeight();
  setTimeout(refreshPlot, 1000);
}, false);

ptypeSelect.addEventListener("change", function(){
  clearTracesOnly();
  recallPlot();
}, false);

/* End of Plotly.js */

/* Tabs */
function registerTab(el){
  el.addEventListener('click', function() {
    let tabLinks = document.getElementsByClassName('tablink');
    for (let i=0; i<tabLinks.length; i++){
      tabLinks[i].className = tabLinks[i].className.replace(' active', '');
    }
    this.className += ' active';
    console.log('Tab '+this.id+' set active.')
    ptypeSelect.value = this.innerText;
    // Compatible way to trigger event for IE11
    let event = document.createEvent("Event");
    event.initEvent('change', false, true); // args: string type, boolean bubbles, boolean cancelable
    ptypeSelect.dispatchEvent(event);    
  }, false);
}

function createTab(tab_id, tab_txt){
  let tab = document.createElement('Button');
  tab.setAttribute('id', tab_id);
  tab.setAttribute('name', tab_id);
  tab.style.width = '20%';
  tab.className = 'tablink';
  tab.appendChild(document.createTextNode(tab_txt));
  return tab;
}
/* End of Tabs */

/* Switch */
function switch_ui(){
  let e = $('#switch_input');
  if (e.prop('checked')){
    app_type = 'tide';
    $('#tide_label').removeClass('off_label').addClass('on_label');
    $('#wave_label').removeClass('on_label').addClass('off_label');
    console.log('Tide Data UI is on.');
  } else {
    app_type = 'wave';
    $('#wave_label').removeClass('off_label').addClass('on_label');
    $('#tide_label').removeClass('on_label').addClass('off_label');
    console.log('Wave Data UI is on.');
  }
  resetPlot(true);
  updateTitleTxt();
  tabs=[]; // Cleanup
  cleanup([mapComp, siteSelect, ptypeSelect, tabContainer]); // Cleanup
  initMap('map');
  init_tabs();
  init_data();
  //console.log('Checkbox switch_input checked: '+e.prop('checked'));
}

function cleanup(elArray){
  for (let el of elArray){
    while (el.firstChild) el.removeChild(el.firstChild); // Cleanup
  }
}

function updateTitleTxt(){
  document.getElementById('titleTxt').innerHTML = 'Queensland Coastline - '+((app_type=='wave') ? 'Wave Buoy' : 'Tide Gauge')+' Open Data';
}
/* End of Switch */
